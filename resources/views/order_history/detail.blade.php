  @extends('template.base')
  @section('plugin-css')
  <link rel="stylesheet" href="{{asset('assets/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slick-carousel/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/examples/css/uikit/carousel.css')}}">
  @endsection
  @section('content')

  <div class="page">
    <div class="page-header">
      <h1 class="page-title"><i class="md-file"></i> Order Detail</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{url('order-history')}}">Order History</a></li>
        <li class="breadcrumb-item active">Order Detail</li>
      </ol>
    </div>
    <div class="page-content container-fluid">
      <div class="card">
        <div class="card-block">
          <div class="row">
            <div class="col-md-6">
              <label>Date</label>
              <p>Wednesday, Agust 3 2017</p>
              <label>Time</label>
              <p>17:00</p>
            </div>
            <div class="col-md-6 text-right">
              <label>Order Code</label>
              <p>ORD/XaAWASAS-QIQ</p>
              <label>Status</label>
              <p>Paid</p>
            </div>
          </div>
          <br>
          <hr>
          <div class="row">
            <div class="col-md-4">
              items
            </div>
            <div class="col-md-3 text-center">
              price/item
            </div>
            <div class="col-md-2 text-center">
              quantity
            </div>
            <div class="col-md-3 text-right">
              total price
            </div>
          </div>
          <hr>
          <div class="row">
            
            <div class="col-md-1">
                <img style="width:100%" src="http://www.semenpadang.co.id/images/kantong-web3.gif"/>
            </div>
            <div class="col-md-3">
              <span style="font-weight: 700;color:#555;font-size:16px">Semen Padang 10 kg</span><br>
            </div>
            <div class="col-md-3 text-center">
              Rp. 70.000/pack
            </div>
            <div class="col-md-2 text-center">
              2
            </div>
            <div class="col-md-3 text-right">
              <span style="font-weight: 600;color:#555;">Rp. 140.000,-</span>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-1">
                <img style="width:100%" src="http://www.semenpadang.co.id/images/kantong-web3.gif"/>
            </div>
            <div class="col-md-3">
              <span style="font-weight: 700;color:#555;font-size:16px">Semen Padang 10 kg</span><br>
            </div>
            <div class="col-md-3 text-center">
              Rp. 70.000/pack
            </div>
            <div class="col-md-2 text-center">
              2
            </div>
            <div class="col-md-3 text-right">
              <span style="font-weight: 600;color:#555;">Rp. 140.000,-</span>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <h4 style="font-weight:600;color:#555">Total</h4>
            </div>
            <div class="col-md-6 text-right">
              <h4 style="font-weight:600;color:#0055F1">Rp. 280.000,-</h4>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>

@endsection
@section('plugin-js')
<script src="{{asset('assets/vendor/switchery/switchery.min.js')}}"></script>
<script src="{{asset('assets/vendor/intro-js/intro.js')}}"></script>
<script src="{{asset('assets/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('assets/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('assets/vendor/slick-carousel/slick.js')}}"></script>
@endsection