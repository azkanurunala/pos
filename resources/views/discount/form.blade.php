  @extends('template.base')

  @section('plugin-css')
  <link rel="stylesheet" href="{{asset('assets/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/select2/select2.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-select/bootstrap-select.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/icheck/icheck.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asrange/asRange.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/ionrangeslider/ionrangeslider.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asspinner/asSpinner.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/clockpicker/clockpicker.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/ascolorpicker/asColorPicker.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-touchspin/bootstrap-touchspin.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/jquery-labelauty/jquery-labelauty.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/jt-timepicker/jquery-timepicker.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/jquery-strength/jquery-strength.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/multi-select/multi-select.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/typeahead-js/typeahead.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/dropify/dropify.css')}}">
  <link rel="stylesheet" href="{{asset('assets/examples/css/forms/advanced.css')}}">
  <script src="{{asset('assets/vendor/breakpoints/breakpoints.js')}}"></script>
  <script>
  Breakpoints();
  </script>
  @endsection

  @section('content')
  <div class="page">
    <div class="page-header">
      <h1 class="page-title"><i class="md-folder"></i> Create  Discount</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{url('discount')}}">Discount</a></li>
        <li class="breadcrumb-item active">Create  Discount</li>
      </ol>
    </div>
    <div class="page-content container-fluid">
      <!-- Panel Select 2 -->
      <div class="panel">

        <div class="panel-body container-fluid">
            <form class="form-horizontal">
              <div class="form-group row">
                <label class="col-md-2 form-control-label">Discount Name </label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="name" placeholder="Input Discount Name Here" autocomplete="off">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 form-control-label">Code </label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="name" placeholder="Input Discount Code Here" autocomplete="off">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 form-control-label">Description </label>
                <div class="col-md-8">
                  <textarea type="text" class="form-control" name="name" placeholder="Input Description Here" autocomplete="off" rows="6"></textarea>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 form-control-label">Type </label>
                <div class="col-md-8">
                  <select class="form-control" name="special-type" data-plugin="select2">
                    <!-- <optgroup label="Alaskan/Hawaiian Time Zone"> -->
                      <option value="AK">Fixed</option>
                      <option value="HI">Percentage</option>
                    <!-- </optgroup> -->
                  </select>
                </div>  
              </div>

              <div class="form-group row">
                <label class="col-md-2 form-control-label">Amount </label>
                <div class="col-md-8">
                  <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="number" class="form-control" placeholder="">
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-md-2 form-control-label">Amount </label>
                <div class="col-md-8">
                  <div class="input-group">
                    <input type="number" class="form-control">
                    <span class="input-group-addon">
                     %
                    </span>
                  </div>
                </div>
                <!-- <div class="col-md-2">
                  <a class="btn btn-default"><i class="md-plus"></i> Add  Category</a>
                </div> -->
              </div>
              
              

            </form>
        </div>
        <hr>
        <div class="panel-footer">
          <div class="row">
            <div class="col-md-12">
              <a class="btn btn-default float-right">Cancel</a>
              <a class="btn btn-success float-right mr-5">Save</a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  @endsection

  @section('plugin-js')
  <script src="{{asset('assets/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/intro-js/intro.js')}}"></script>
  <script src="{{asset('assets/vendor/screenfull/screenfull.js')}}"></script>
  <script src="{{asset('assets/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
  <script src="{{asset('assets/vendor/select2/select2.full.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
  <script src="{{asset('assets/vendor/icheck/icheck.min.js')}}"></script>
  <script src="{{asset('assets/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/asrange/jquery-asRange.min.js')}}"></script>
  <script src="{{asset('assets/vendor/ionrangeslider/ion.rangeSlider.min.js')}}"></script>
  <script src="{{asset('assets/vendor/asspinner/jquery-asSpinner.min.js')}}"></script>
  <script src="{{asset('assets/vendor/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
  <script src="{{asset('assets/vendor/ascolor/jquery-asColor.min.js')}}"></script>
  <script src="{{asset('assets/vendor/asgradient/jquery-asGradient.min.js')}}"></script>
  <script src="{{asset('assets/vendor/ascolorpicker/jquery-asColorPicker.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>
  <script src="{{asset('assets/vendor/jquery-knob/jquery.knob.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>
  <script src="{{asset('assets/vendor/jquery-labelauty/jquery-labelauty.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('assets/vendor/jt-timepicker/jquery.timepicker.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datepair/datepair.min.js')}}"></script>
  <script src="{{asset('assets/vendor/datepair/jquery.datepair.min.js')}}"></script>
  <script src="{{asset('assets/vendor/jquery-strength/password_strength.js')}}"></script>
  <script src="{{asset('assets/vendor/jquery-strength/jquery-strength.min.js')}}"></script>
  <script src="{{asset('assets/vendor/multi-select/jquery.multi-select.js')}}"></script>
  <script src="{{asset('assets/vendor/typeahead-js/bloodhound.min.js')}}"></script>
  <script src="{{asset('assets/vendor/typeahead-js/typeahead.jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
  @endsection
  @section('page-js')
  <!-- <script src="{{asset('assets/js/Site.js')}}"></script> -->
  <script src="{{asset('assets/js/Plugin/asscrollable.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/slidepanel.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/switchery.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/select2.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/bootstrap-tokenfield.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/bootstrap-tagsinput.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/bootstrap-select.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/icheck.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/switchery.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/asrange.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/ionrangeslider.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/asspinner.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/clockpicker.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/ascolorpicker.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/bootstrap-maxlength.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/jquery-knob.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/bootstrap-touchspin.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/card.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/jquery-labelauty.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/jt-timepicker.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/datepair.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/jquery-strength.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/multi-select.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/jquery-placeholder.js')}}"></script>
  <script src="{{asset('assets/vendor/dropify/dropify.min.js')}}"></script>
  <!-- <script src="{{asset('assets/examples/js/forms/advanced.js')}}"></script> -->
  @endsection

