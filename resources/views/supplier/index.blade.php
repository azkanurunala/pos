  @extends('template.base')

  @section('plugin-css')
   <link rel="stylesheet" href="{{asset('assets/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-bs4/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('assets/examples/css/tables/datatable.css')}}">
  @endsection

  @section('content')
  <div class="page">
    <div class="page-header">
      <h1 class="page-title"><i class="md-folder"></i> Supplier</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
        <li class="breadcrumb-item active">Supplier</li>
      </ol>
      <div class="page-header-actions">
        <a class="btn btn-md btn-primary btn-round" href="{{url('supplier/form')}}">
          <i class="icon md-plus-circle-o" aria-hidden="true"></i>
          <span class="hidden-sm-down">New Supplier</span>
        </a>
      </div>
    </div>
    <div class="page-content">
      <!-- Panel Basic -->
      <div class="panel">
        
        <div class="panel-body">
          <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Action</th>
              </tr>
            </tfoot>
            <tbody>
              <tr class="odd">
                <td>1</td>
                <td>
                  Supplier Semen Jogja
                </td>
                <td>
                  +6285725778315  
                </td>
                <td>
                  semen@jogja.co.id
                </td>
                <td>Jl. Taman Duta 1 no.2 Depok, Yogyakarta</td>
                <td>
                <!-- <a class="btn btn-success mb-5"><i class="md-eye"></i> Detail</a> -->
                  <a class="btn btn-primary mb-5" href="{{url('supplier/form')}}"><i class="md-edit"></i> Edit</a>
                  <a class="btn btn-danger mb-5" data-toggle="modal" data-target="#deleteModal"><i class="md-delete"></i> Delete</a>
                </td>
              </tr>
              
            </tbody>
          </table>
        </div>
      </div>
      <!-- End Panel Basic -->
    </div>
  </div>

  @endsection

  @section('modal')
    @include('modal.supplier.modal_delete')
  @endsection

  @section('plugin-js')
  <script src="{{asset('assets/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/intro-js/intro.js')}}"></script>
  <script src="{{asset('assets/vendor/screenfull/screenfull.js')}}"></script>
  <script src="{{asset('assets/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net/jquery.dataTables.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-rowgroup/dataTables.rowGroup.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-scroller/dataTables.scroller.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-select-bs4/dataTables.select.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-responsive/dataTables.responsive.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons/dataTables.buttons.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons/buttons.html5.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons/buttons.flash.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons/buttons.print.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons/buttons.colVis.js')}}"></script>
  <script src="{{asset('assets/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js')}}"></script>
  <script src="{{asset('assets/vendor/asrange/jquery-asRange.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootbox/bootbox.js')}}"></script>
  @endsection

