  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">
            <!-- <li class="site-menu-category">General</li> -->

            <li class="site-menu-item {{ (\Request::is('/'))?'active':'' }}">
              <a class="animsition-link" href="{{url('/')}}">
                <i class="site-menu-icon md-view-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
              </a>
            </li>

            <li class="site-menu-item {{ (\Request::is('catalog'))?'active':'' }}">
              <a class="animsition-link" href="{{url('catalog')}}">
                <i class="site-menu-icon md-shopping-basket" aria-hidden="true"></i>
                <span class="site-menu-title">Catalog</span>
              </a>
            </li>

            <li class="site-menu-item {{ (\Request::is('product*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-folder" aria-hidden="true"></i>
                <span class="site-menu-title">Product</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('product')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Products</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('product/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Product</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('category*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-apps" aria-hidden="true"></i>
                <span class="site-menu-title">Category</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('category')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Categories</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('category/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Category</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('attribute*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-view-toc" aria-hidden="true"></i>
                <span class="site-menu-title">Attribute</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('attribute')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Attributes</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('attribute/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Attribute</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('special-type*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-star" aria-hidden="true"></i>
                <span class="site-menu-title">Special Type</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('special-type')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Special Types</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('special-type/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Special Type</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('brand*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-label" aria-hidden="true"></i>
                <span class="site-menu-title">Brand</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('brand')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Brands</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('brand/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Brand</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('discount*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-ticket-star" aria-hidden="true"></i>
                <span class="site-menu-title">Discount</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('discount')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Discounts</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('discount/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Discount</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('order-history*'))?'active':'' }}">
              <a class="animsition-link" href="{{url('order-history')}}">
                <i class="site-menu-icon md-file" aria-hidden="true"></i>
                <span class="site-menu-title">Order History</span>
              </a>
            </li>

            <li class="site-menu-item {{ (\Request::is('purchase-order*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-arrow-left-bottom" aria-hidden="true"></i>
                <span class="site-menu-title">Purchase Order</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('purchase-order')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Purchase Orders</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('purchase-order/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Purchase Order</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('transfer-order*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-arrow-right-top" aria-hidden="true"></i>
                <span class="site-menu-title">Transfer Order</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('transfer-order')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Transfer Orders</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('transfer-order/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Transfer Order</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('sales*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-trending-up" aria-hidden="true"></i>
                <span class="site-menu-title">Sales</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('sales')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">Sales Report</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('sales/income')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">Income Report</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('sales/expense')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">Expense Report</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="site-menu-item {{ (\Request::is('supplier*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-truck" aria-hidden="true"></i>
                <span class="site-menu-title">Supplier</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('supplier')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Suppliers</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('supplier/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Supplier</span>
                  </a>
                </li>
              </ul>
            </li>
            
            <li class="site-menu-item {{ (\Request::is('distributor*'))?'active':'' }} has-sub">
              <a href="javascript:void(0)">
                <i class="site-menu-icon md-arrows" aria-hidden="true"></i>
                <span class="site-menu-title">Distributor</span>
                <span class="site-menu-arrow"></span>
              </a>
              <ul class="site-menu-sub">
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('distributor')}}">
                    <i class="site-menu-icon md-view-list" aria-hidden="true"></i>
                    <span class="site-menu-title">All Distributors</span>
                  </a>
                </li>
                <li class="site-menu-item">
                  <a class="animsition-link" href="{{url('distributor/form')}}">
                    <i class="site-menu-icon md-collection-plus" aria-hidden="true"></i>
                    <span class="site-menu-title">New Distributor</span>
                  </a>
                </li>
              </ul>
            </li>

          </ul>
        </div>
      </div>
    </div>
  </div>