  @extends('template.base')
  @section('plugin-css')
  <link rel="stylesheet" href="{{asset('assets/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slick-carousel/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/examples/css/uikit/carousel.css')}}">
  @endsection
  @section('content')

  <div class="page">
    <div class="page-content container-fluid">
      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-md-4">
          <div class="carousel slide" id="exampleCarouselDefault" data-ride="carousel">
            <ol class="carousel-indicators carousel-indicators-fall">
              <li class="active" data-slide-to="0" data-target="#exampleCarouselDefault"></li>
              <li data-slide-to="1" data-target="#exampleCarouselDefault"></li>
              <li data-slide-to="2" data-target="#exampleCarouselDefault"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img class="w-full" src="http://www.signalreadymix.co/wp-content/uploads/2016/11/harga-semen-perekat-hebel.jpg"" alt="..." />
              </div>
              <div class="carousel-item">
                <img class="w-full" src="http://www.signalreadymix.co/wp-content/uploads/2016/11/harga-semen-perekat-hebel.jpg"" alt="..." />
              </div>
              <div class="carousel-item">
                <img class="w-full" src="http://www.signalreadymix.co/wp-content/uploads/2016/11/harga-semen-perekat-hebel.jpg" alt="..." />
              </div>
            </div>
            <a class="carousel-control-prev" href="#exampleCarouselDefault" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon md-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#exampleCarouselDefault" role="button" data-slide="next">
              <span class="carousel-control-next-icon md-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-bordered" style="border-top-right-radius: 0;border-top-left-radius: 0;">
                  <div class="panel-body">
                      <h5 class="text-uppercase">Attributes</h5>
                      <span class="badge badge-lg bg-cyan-500">Color: White, Green, Blue</span>
                      <span class="badge badge-lg bg-cyan-500">Size: XL, L, S</span>
                      <span class="badge badge-lg bg-cyan-500">Preferences</span>
                      <hr>
                      <h5 class="text-uppercase">Special Types</h5>
                      <span class="badge badge-lg bg-blue-grey-600">Recommended</span>
                      <span class="badge badge-lg bg-blue-grey-600">Top Seller</span>
                      <span class="badge badge-lg bg-blue-grey-600">Best Deals</span>
                      <hr>
                      <h5 class="text-uppercase">SKU Number : <label>1213131313131313</label></h5>
                  </div>
                  <div class="panel-footer">
                    <div class="media">
                      <div class="pr-20">
                        <a class="avatar avatar-lg" href="javascript:void(0)">
                          <img src="{{asset('assets/portraits/2.jpg')}}" alt="...">
                        </a>
                      </div>
                      <div class="media-body mt-15" style="font-weight:600;color:#0055F1">
                        Holcim Semen Terbaik
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-bordered">
              <div class="panel-heading">
                <h3 class="panel-title">
                  <strong style="font-weight:600;color:#0055F1">Perekat Bata Holcim</strong>
                  <a style="margin-top:-7px" class="btn btn-danger mb-5 float-right"><i class="md-delete"></i> Delete</a>
                  <a style="margin-top:-7px" class="btn btn-primary mb-5 float-right mr-5"><i class="md-edit"></i> Edit</a>
                </h3>
                <label class="pl-30 pr-30"><a>Semen dan Perekat Bata</a> > <a>Perekat Bata</a></label>

              </div>
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <h5 class="text-uppercase">Description</h5>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id sapien posuere sem tempus volutpat blandit eu nulla. Integer congue rutrum metus vel consequat. Praesent lacinia ligula nisl, ac dapibus diam convallis id. Vestibulum blandit molestie turpis, quis sollicitudin velit blandit nec. Sed velit lacus, congue et aliquam pretium, consectetur non purus. Maecenas condimentum, urna at luctus consectetur, velit tortor feugiat dolor, sed rutrum nunc felis id nisi. Sed vulputate ut ante quis tincidunt.
                        </p>
                        <hr>
                        <h5 class="text-uppercase">Weight</h5>
                        <p>10 kg</p>
                        <hr>
                        <h5 class="text-uppercase">Dimension</h5>
                        <p>100 cm x 10cm x 60cm</p>

                    </div>
                    <div class="col-md-4">
                        <h5 class="text-uppercase">Buying Price</h5>
                        <span class="badge badge-lg badge-primary">Rp. 150.000,00</span>
                        <hr>
                        <h5 class="text-uppercase">Selling Price</h5>
                        <span class="badge badge-lg badge-success">Rp. 180.000,00</span>
                        <hr>
                        <h5 class="text-uppercase">Discount</h5>
                        <span class="badge badge-lg bg-grey-500">XQGWRT | 10%</span>
                        <span class="badge badge-lg bg-grey-500">XASDRT | 10.000</span>
                        <span class="badge badge-lg bg-grey-500">XQSSDE | 50.000</span>
                        <hr>
                        <h5 class="text-uppercase">Stock Quantity</h5>
                        <span>124 in stock</span>

                    </div>
                </div>
              </div>
              <!-- <div class="panel-footer">
              </div> -->
            </div>
          </div>
      </div>
    </div>
  </div>

@endsection
@section('plugin-js')
<script src="{{asset('assets/vendor/switchery/switchery.min.js')}}"></script>
<script src="{{asset('assets/vendor/intro-js/intro.js')}}"></script>
<script src="{{asset('assets/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('assets/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('assets/vendor/slick-carousel/slick.js')}}"></script>
@endsection