  @extends('template.base')

  @section('plugin-css')
  <link rel="stylesheet" href="{{asset('assets/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/chartist/chartist.css')}}">
  <link rel="stylesheet" href="{{asset('assets/examples/css/widgets/chart.css')}}">
  @endsection

  @section('page-css')
    <style>
    .animsition, .animsition-overlay{
      opacity: 1;
    }
    .col-lg-6{
      display: inline-block;
      float:left;
    }
    .counter-md .counter-number-group, .counter-md>.counter-number{
      color:#0055F1;
      font-weight: 500;
    }

    .stat-action{
      background:#0040FF;
      width:40%;
      position: relative;
      padding:30px
    }

    .stat-action .stat-bg-icon{
      /*color:#0055F1;*/
      color:rgba(255,255,255,0.15);
      position: absolute;
      bottom: 10px;
      right: 20px;
      font-size:70px;
    }

    .stat-action  .stat-link{
      position: absolute;
      font-size: 16px;
      left:20px;
      top: 50%;
      margin-top: -25px;
    }

    .stat-action .stat-link-icon{
      position: absolute;
      font-size: 36px;
      right: 16px;
      top: 50%;
      margin-top: -28px;
    }
    .stat-action  .stat-link:hover,
    .stat-action  .stat-link-icon:hover{
        color: #eee;
        text-decoration: none;
    }

    .card-title{
       padding: 20px 20px 10px 20px;
       margin: 0;
       font-size: 15px;
       color: #555;
       font-weight: 600;
       text-transform: uppercase;
    }
    .dashboard label{
      color:#0055F1;
    }
    .nav-tabs-line .nav-link {
      padding: .715rem 1.075rem;
      border-bottom: 2px solid transparent;
    }

    .dashboard-top{
      padding: 15px 30px;
      margin:0;
    }
    .dashboard-top li{
      font-size: 16px;
      font-weight: 600;
    }

    .dashboard-top li small{
      color:#0055F1;
      font-size: 14px;
    }

    .block-bottom-link {
      background: #1055ff;
      font-size: 18px;
      border-bottom-left-radius: 5px;
      border-bottom-right-radius: 5px;
    }
    .block-bottom-link a {
      color:#fff;
    }
    .block-bottom-link a:hover {
      color:#eee;
      text-decoration: none;
    }

    .dashboard .card, .dashboard .panel {
        height: -webkit-calc(100% - 30px);
        height: calc(100% - 30px);
        margin-bottom: 30px;
        box-shadow: 1px 1px 3px #ccc;
    }
  </style>
  @endsection

  @section('content')

  <div class="page">
    <div class="page-content container">
      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-lg-3">
          <div class="card card-shadow">
            <div class="card-header card-header-transparent cover overlay">
              <div class="cover-background h-120" style="background-image: url('{{asset('assets/photos/placeholder.png')}}')"></div>
            </div>
            <div class="card-block px-30 py-20" style="height:calc(100% - 250px);">
              <div style="position:relative;">
                <a class="avatar avatar-100 bg-white img-bordered" href="javascript:void(0)" style="position:absolute;top:-80px;left:50%;margin-left:-50px">
                  <img src="{{asset('assets/portraits/2.jpg')}}" alt="">
                </a>
              </div>
              <div class="mb-10 mt-30 text-center">
                <div class="font-size-20 font-weight-700">Toko GW</div>
                <div class="font-size-14">Jakarta, Indonesia</div>
                <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                  <span class="text-uppercase">Manage Store</span>
                  <i class="icon md-chevron-right" aria-hidden="true"></i>
                </a>
              </div>
            </div>
            
          </div>
        </div>
        <div class="col-lg-9">

          <div class="col-lg-6">

            <div class="card p-0 flex-row justify-content-between">
              <div class="counter p-30 counter-md text-left">
                <div class="counter-number-group">
                  <span class="counter-number">450.000</span>
                </div>
                <div class="counter-label text-capitalize font-size-20"><i class="md-folder"></i> Products</div>
              </div>
              <div class="stat-action" style="">
                <i class="icon md-folder stat-bg-icon" aria-hidden="true"></i>
                <a class="white text-uppercase stat-link" href="{{url('product')}}">
                  Manage<br>
                  Product
                </a>
                <a class="stat-link-icon" href="{{url('product')}}"><i class="white md-chevron-right"></i></a>
              </div>
            </div>

            <div class="card p-0 flex-row justify-content-between">
              <div class="counter p-30 counter-md text-left">
                <div class="counter-number-group">
                  <span class="counter-number">145</span>
                </div>
                <div class="counter-label text-capitalize font-size-20"><i class="md-apps"></i> Categories</div>
              </div>
              <div class="stat-action" style="">
                <i class="icon md-apps stat-bg-icon" aria-hidden="true"></i>
                <a class="white text-uppercase stat-link" href="{{url('category')}}">
                  Manage<br>
                  Category
                </a>
                <a class="stat-link-icon" href="{{url('category')}}"><i class="white md-chevron-right"></i></a>
              </div>
            </div>
            

          </div>

          <div class="col-lg-6">

            <div class="card p-0 flex-row justify-content-between">
              <div class="counter p-30 counter-md text-left">
                <div class="counter-number-group">
                  <span class="counter-number">342</span>
                </div>
                <div class="counter-label text-capitalize font-size-20"><i class="md-label"></i> Brands</div>
              </div>
              <div class="stat-action" style="">
                <i class="icon md-label stat-bg-icon" aria-hidden="true"></i>
                <a class="white text-uppercase stat-link" href="{{url('brand')}}">
                  Manage<br>
                  Brand
                </a>
                <a class="stat-link-icon" href="{{url('brand')}}"><i class="white md-chevron-right"></i></a>
              </div>
            </div>

            <div class="card p-0 flex-row justify-content-between">
              <div class="counter p-30 counter-md text-left">
                <div class="counter-number-group">
                  <span class="counter-number">12.376</span>
                </div>
                <div class="counter-label text-capitalize font-size-20"><i class="md-file"></i> Orders</div>
              </div>
              <div class="stat-action" style="">
                <i class="icon md-file stat-bg-icon" aria-hidden="true"></i>
                <a class="white text-uppercase stat-link" href="{{url('order-history')}}">
                  Order<br>
                  History
                </a>
                <a class="stat-link-icon" href="{{url('category')}}"><i class="white md-chevron-right"></i></a>
              </div>
            </div>
            

          </div>




        </div>
      </div>


      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-lg-6">
          <div class="card card-shadow" id="chartLinebarLarge">
            <div class="card-heading" >
              <div class="row">
                <div class="col-md-4">
                  <h4 class="card-title">Sales</h4>
                </div>
              <!-- </div>
              <hr style="margin:0;padding:0">
              <div class="row"> -->
                <div class="col-md-8">
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist" style="padding:10px 0 0 0;border:none">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab">Daily</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab">Weekly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab">Monthly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab">Overall</a></li>
                  </ul>
                </div>
              </div>
              <hr style="margin:0;padding:0">
            </div>
            <div class="card-block p-0 h-full">
              <div class="p-30" style="calc(100% - 260px);">
                <div class="ct-chart chart-line h-300"></div>
              </div>
              <div class="p-30 bg-grey-100">
                <div class="row">
                  <div class="col-md-6">
                    <div class="font-size-20 grey-700 mb-5">Rp.105.6 M</div>
                    <div class="font-size-14">Today's Revenue</div>
                  </div>
                  <div class="col-md-6">
                    <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                      <span class="text-uppercase" style="font-size: 18px">Full Report</span>
                      <i class="icon md-chevron-right" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card card-shadow" id="chartLinebarLarge1">
            <div class="card-heading" >
              <div class="row">
                <div class="col-md-4">
                  <h4 class="card-title">Profit</h4>
                </div>
              <!-- </div>
              <hr style="margin:0;padding:0">
              <div class="row"> -->
                <div class="col-md-8">
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist" style="padding:10px 0 0 0;border:none">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab">Daily</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab">Weekly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab">Monthly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab">Overall</a></li>
                  </ul>
                </div>
              </div>
              <hr style="margin:0;padding:0">
            </div>
            <div class="card-block p-0 h-full">
              <div class="p-30" style="calc(100% - 260px);">
                <div class="ct-chart chart-line h-300"></div>
              </div>
              <div class="p-30 bg-grey-100">
                <div class="row">
                  <div class="col-md-6">
                    <div class="font-size-20 grey-700 mb-5">Rp.50.6 M</div>
                    <div class="font-size-14">Today's Profit</div>
                  </div>
                  <div class="col-md-6">
                    <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                      <span class="text-uppercase" style="font-size: 18px">Full Report</span>
                      <i class="icon md-chevron-right" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card card-shadow chart-line-bar-large" id="chartLinebarLarge2">
            <div class="card-heading" >
              <div class="row">
                <div class="col-md-4">
                  <h4 class="card-title">Income</h4>
                </div>
              <!-- </div>
              <hr style="margin:0;padding:0">
              <div class="row"> -->
                <div class="col-md-8">
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist" style="padding:10px 0 0 0;border:none">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab">Daily</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab">Weekly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab">Monthly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab">Overall</a></li>
                  </ul>
                </div>
              </div>
              <hr style="margin:0;padding:0">
            </div>
            <div class="card-block p-0 h-full">
              <div class="p-30" style="calc(100% - 260px);">
                <div class="ct-chart chart-line h-300"></div>
              </div>
              <div class="p-30 bg-grey-100">
                <div class="row">
                  <div class="col-md-6">
                    <div class="font-size-20 grey-700 mb-5">Rp.40 M</div>
                    <div class="font-size-14">Today's Other Income</div>
                  </div>
                  <div class="col-md-6">
                    <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                      <span class="text-uppercase" style="font-size: 18px">Full Report</span>
                      <i class="icon md-chevron-right" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="card card-shadow chart-line-bar-large" id="chartLinebarLarge3">
            <div class="card-heading" >
              <div class="row">
                <div class="col-md-4">
                  <h4 class="card-title">Expense</h4>
                </div>
              <!-- </div>
              <hr style="margin:0;padding:0">
              <div class="row"> -->
                <div class="col-md-8">
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist" style="padding:10px 0 0 0;border:none">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab">Daily</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab">Weekly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab">Monthly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab">Overall</a></li>
                  </ul>
                </div>
              </div>
              <hr style="margin:0;padding:0">
            </div>
            <div class="card-block p-0 h-full">
              <div class="p-30" style="calc(100% - 260px);">
                <div class="ct-chart chart-line h-300"></div>
              </div>
              <div class="p-30 bg-grey-100">
                <div class="row">
                  <div class="col-md-6">
                    <div class="font-size-20 grey-700 mb-5">Rp.15 M</div>
                    <div class="font-size-14">Spent Today</div>
                  </div>
                  <div class="col-md-6">
                    <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                      <span class="text-uppercase" style="font-size: 18px">Full Report</span>
                      <i class="icon md-chevron-right" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-lg-4">
          <div class="card card-shadow" id="chartLinebarLarge">
            <div class="card-heading" >
                  <h4 class="card-title text-center">Top 5 Products</h4>
              <!-- </div>
              <hr style="margin:0;padding:0">
              <div class="row"> -->
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist" style="padding:10px 0 0 0;border:none">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab">Daily</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab">Weekly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab">Monthly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab">Overall</a></li>
                  </ul>
              <hr style="margin:0;padding:0">
            </div>
            <div class="card-block p-0 h-full">
              <div class="p-10">
                <ol class="dashboard-top">
                  <li>Perekat Bata Mortar Utama <br><small>( 150 items sold )</small></li>
                  <hr>
                  <li>Semen Holcim <br><small>( 100 items sold )</small></li>
                  <hr>
                  <li>Genting Merah <br><small>( 80 items sold )</small></li>
                  <hr>
                  <li>Seng Mantap <br><small>( 70 items sold )</small></li>
                  <hr>
                  <li>Pasir Merah <br><small>( 50 items sold )</small></li>
                </ol>
              </div>
              <div class="p-15 text-center block-bottom-link">
                <a href="#" class="text-uppercase">See Full Report <i class="md-chevron-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card card-shadow" id="chartLinebarLarge">
            <div class="card-heading" >
                  <h4 class="card-title text-center">Top 5 Categories</h4>
              <!-- </div>
              <hr style="margin:0;padding:0">
              <div class="row"> -->
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist" style="padding:10px 0 0 0;border:none">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab">Daily</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab">Weekly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab">Monthly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab">Overall</a></li>
                  </ul>
              <hr style="margin:0;padding:0">
            </div>
            <div class="card-block p-0 h-full">
              <div class="p-10">
                <ol class="dashboard-top">
                  <li>Perekat Bata Mortar Utama <br><small>( 150 items sold )</small></li>
                  <hr>
                  <li>Semen Holcim <br><small>( 100 items sold )</small></li>
                  <hr>
                  <li>Genting Merah <br><small>( 80 items sold )</small></li>
                  <hr>
                  <li>Seng Mantap <br><small>( 70 items sold )</small></li>
                  <hr>
                  <li>Pasir Merah <br><small>( 50 items sold )</small></li>
                </ol>
              </div>
              <div class="p-15 text-center block-bottom-link">
                <a href="#" class="text-uppercase">See Full Report <i class="md-chevron-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card card-shadow" id="chartLinebarLarge">
            <div class="card-heading" >
                  <h4 class="card-title text-center">Top 5 Brands</h4>
              <!-- </div>
              <hr style="margin:0;padding:0">
              <div class="row"> -->
                  <ul class="nav nav-tabs nav-tabs-line" role="tablist" style="padding:10px 0 0 0;border:none">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab">Daily</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab">Weekly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab">Monthly</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab">Overall</a></li>
                  </ul>
              <hr style="margin:0;padding:0">
            </div>
            <div class="card-block p-0 h-full">
              <div class="p-10">
                <ol class="dashboard-top">
                  <li>Mortar Utama <br><small>( 150 items sold )</small></li>
                  <hr>
                  <li>Holcim <br><small>( 100 items sold )</small></li>
                  <hr>
                  <li>Merah <br><small>( 80 items sold )</small></li>
                  <hr>
                  <li>Mantap <br><small>( 70 items sold )</small></li>
                  <hr>
                  <li>Pasir-M<br><small>( 50 items sold )</small></li>
                </ol>
              </div>
              <div class="p-15 text-center block-bottom-link">
                <a href="#" class="text-uppercase">See Full Report <i class="md-chevron-right"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-md-4">
          <div class="card">
            <div class="card-heading">
              <h3 class="card-title" style="font-size:18px;font-weight: 500">Last Order</h3>
            </div>
            <div class="card-block">
              <div class="row">
                  <div class="col-md-6">
                    <label>Date</label>
                    <p>Agust 3 2017</p>
                    <label>Time</label>
                    <p>17:00</p>
                  </div>
                  <div class="col-md-6 text-right">
                    <label>Order Code</label>
                    <p>ORD/XaAWASAS</p>
                    <label>Status</label>
                    <p>Paid</p>
                  </div>
              </div>
              <br>
              <label>Items</label>
              <hr>
              <div class="row">
                <div class="col-md-5">
                  items
                </div>
                <div class="col-md-2 text-center">
                  qty
                </div>
                <div class="col-md-5 text-right">
                  price
                </div>
              </div>
              <hr>
              <div class="row">
                
                <div class="col-md-5">
                  <span style="font-weight: 700;color:#555;font-size:14px">Semen Padang 10 kg</span><br>
                  Rp.70.000/pack
                </div>
                <div class="col-md-2 text-center">
                  2
                </div>
                <div class="col-md-5 text-right">
                  <span style="font-weight: 600;color:#555;">Rp.140.000,-</span>
                </div>
              </div>
              <hr>
              <div class="row">
                
                <div class="col-md-5">
                  <span style="font-weight: 700;color:#555;font-size:14px">Semen Padang 10 kg</span><br>
                  Rp.70.000/pack
                </div>
                <div class="col-md-2 text-center">
                  2
                </div>
                <div class="col-md-5 text-right">
                  <span style="font-weight: 600;color:#555;">Rp.140.000,-</span>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <h4 style="font-weight:600;color:#555">Total</h4>
                </div>
                <div class="col-md-6 text-right">
                  <h4 style="font-weight:600;color:#0055F1">Rp.280.000,-</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-md-10 offset-md-1">
                  <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                    <span class="text-uppercase" style="font-size: 18px">Order History</span>
                    <i class="icon md-chevron-right" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-heading">
              <h3 class="card-title" style="font-size:18px;font-weight: 500">Last Purchase Order</h3>
            </div>
            <div class="card-block">
              <div class="row">
                <div class="col-md-6">
                  <label>Title </label>
                  <p >Purchase Raw Material</p>
                  <label>Supplier </label>
                  <p >Pusat Semen Jogja</p>
                </div>
                <div class="col-md-6">
                  <label>Purchase Date </label>
                  <p >Wednesday, September 10, 2017</p>
                  <label>Purchase Time </label>
                  <p >10:00 AM</p>
                </div>
              </div>
              <br>
              <label>Purchase Items </label>

              <hr>

              <div class="row">
                <div class="col-md-5">
                  items
                </div>
                <div class="col-md-2 text-center">
                  qty
                </div>
                <div class="col-md-5 text-right">
                  price
                </div>
              </div>
              <hr>
              <div class="row">
                
                <div class="col-md-5">
                  <span style="font-weight: 700;color:#555;font-size:14px">Semen Padang 10 kg</span><br>
                  Rp.70.000/pack
                </div>
                <div class="col-md-2 text-center">
                  2
                </div>
                <div class="col-md-5 text-right">
                  <span style="font-weight: 600;color:#555;">Rp.140.000,-</span>
                </div>
              </div>
              <hr>
              <div class="row">
                
                <div class="col-md-5">
                  <span style="font-weight: 700;color:#555;font-size:14px">Semen Padang 10 kg</span><br>
                  Rp.70.000/pack
                </div>
                <div class="col-md-2 text-center">
                  2
                </div>
                <div class="col-md-5 text-right">
                  <span style="font-weight: 600;color:#555;">Rp.140.000,-</span>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <h4 style="font-weight:600;color:#555">Total</h4>
                </div>
                <div class="col-md-6 text-right">
                  <h4 style="font-weight:600;color:#0055F1">Rp.280.000,-</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                    <span class="text-uppercase" style="font-size: 18px">All Purchase Orders</span>
                    <i class="icon md-chevron-right" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-heading">
              <h3 class="card-title" style="font-size:18px;font-weight: 500">Last Transfer Order</h3>
            </div>
            <div class="card-block">
              <div class="row">
                <div class="col-md-6">
                  <label>Title </label>
                  <p >Transfer Raw Material</p>
                  <label>Distributor </label>
                  <p >Toko Abang</p>
                </div>
                <div class="col-md-6">
                  <label>Transfer Date </label>
                  <p >Wednesday, September 10, 2017</p>
                  <label>Transfer Time </label>
                  <p >10:00 AM</p>
                </div>
              </div>
              <br>
              <label>Transfer Items </label>

              <hr>

              <div class="row">
                <div class="col-md-5">
                  items
                </div>
                <div class="col-md-2 text-center">
                  qty
                </div>
                <div class="col-md-5 text-right">
                  price
                </div>
              </div>
              <hr>
              <div class="row">
                
                <div class="col-md-5">
                  <span style="font-weight: 700;color:#555;font-size:14px">Semen Padang 10 kg</span><br>
                  Rp.70.000/pack
                </div>
                <div class="col-md-2 text-center">
                  2
                </div>
                <div class="col-md-5 text-right">
                  <span style="font-weight: 600;color:#555;">Rp.140.000,-</span>
                </div>
              </div>
              <hr>
              <div class="row">
                
                <div class="col-md-5">
                  <span style="font-weight: 700;color:#555;font-size:14px">Semen Padang 10 kg</span><br>
                  Rp.70.000/pack
                </div>
                <div class="col-md-2 text-center">
                  2
                </div>
                <div class="col-md-5 text-right">
                  <span style="font-weight: 600;color:#555;">Rp.140.000,-</span>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <h4 style="font-weight:600;color:#555">Total</h4>
                </div>
                <div class="col-md-6 text-right">
                  <h4 style="font-weight:600;color:#0055F1">Rp.280.000,-</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <a class="btn btn-md btn-primary btn-round waves-effect waves-classic" style="display: block;margin:15px auto 0 auto">
                    <span class="text-uppercase" style="font-size: 18px">All Transfer Orders</span>
                    <i class="icon md-chevron-right" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('plugin-js')
  <script src="{{asset('assets/vendor/switchery/switchery.min.js')}}	"></script>
  <script src="{{asset('assets/vendor/intro-js/intro.js')}}	"></script>
  <script src="{{asset('assets/vendor/screenfull/screenfull.js')}}	"></script>
  <script src="{{asset('assets/vendor/slidepanel/jquery-slidePanel.js')}}	"></script>
  <script src="{{asset('assets/vendor/sparkline/jquery.sparkline.min.js')}}	"></script>
  <script src="{{asset('assets/vendor/chartist/chartist.min.js')}}	"></script>
  <script src="{{asset('assets/vendor/matchheight/jquery.matchHeight-min.js')}}	"></script>
  <!-- <script src="{{asset('assets/examples/js/widgets/chart.js ')}}"></script> -->

@endsection
@section('page-js')
  <script src="{{asset('assets/js/Site.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/asscrollable.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/slidepanel.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/switchery.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/matchheight.js')}}"></script>
  <Script type="text/javascript">
    // (function() {
    var labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var series = [20, 50, 70, 110, 100, 200, 230, 50, 80, 140, 130, 150];
    new Chartist.Line("#chartLinebarLarge .chart-line", {
      labels: labels,
      series: [
        series
      ]
    }, {
      low: 0,
      showArea: false,
      showPoint: false,
      showLine: true,
      lineSmooth: false,
      fullWidth: true,
      chartPadding: {
        top: 0,
        right: 10,
        bottom: 0,
        left: 10
      },
      axisX: {
        showLabel: true,
        showGrid: false,
        offset: 30
      },
      axisY: {
        showLabel: true,
        showGrid: true,
        offset: 30
      }
    });

    new Chartist.Line("#chartLinebarLarge1 .chart-line", {
      labels: labels,
      series: [
        series
      ]
    }, {
      low: 0,
      showArea: false,
      showPoint: false,
      showLine: true,
      lineSmooth: false,
      fullWidth: true,
      chartPadding: {
        top: 0,
        right: 10,
        bottom: 0,
        left: 10
      },
      axisX: {
        showLabel: true,
        showGrid: false,
        offset: 30
      },
      axisY: {
        showLabel: true,
        showGrid: true,
        offset: 30
      }
    });

    new Chartist.Line("#chartLinebarLarge2 .chart-line", {
      labels: labels,
      series: [
        series
      ]
    }, {
      low: 0,
      showArea: false,
      showPoint: false,
      showLine: true,
      lineSmooth: false,
      fullWidth: true,
      chartPadding: {
        top: 0,
        right: 10,
        bottom: 0,
        left: 10
      },
      axisX: {
        showLabel: true,
        showGrid: false,
        offset: 30
      },
      axisY: {
        showLabel: true,
        showGrid: true,
        offset: 30
      }
    });

    new Chartist.Line("#chartLinebarLarge3 .chart-line", {
      labels: labels,
      series: [
        series
      ]
    }, {
      low: 0,
      showArea: false,
      showPoint: false,
      showLine: true,
      lineSmooth: false,
      fullWidth: true,
      chartPadding: {
        top: 0,
        right: 10,
        bottom: 0,
        left: 10
      },
      axisX: {
        showLabel: true,
        showGrid: false,
        offset: 30
      },
      axisY: {
        showLabel: true,
        showGrid: true,
        offset: 30
      }
    });

  </Script>
@endsection