<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>ToBaPOS @yield('title')</title>
  <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap-extend.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{asset('assets/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/chartist/chartist.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/jvectormap/jquery-jvectormap.css')}}">
  <link rel="stylesheet" href="{{asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css')}}">
  <link rel="stylesheet" href="{{asset('assets/examples/css/dashboard/v1.css')}}">
  @yield('plugin-css')
  <!-- Fonts -->
  <link rel="stylesheet" href="{{asset('assets/fonts/material-design/material-design.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/fonts/brand-icons/brand-icons.min.css')}}">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <!--[if lt IE 9]>
    <script src="{{asset('assets/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="{{asset('assets/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('assets/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="{{asset('assets/vendor/breakpoints/breakpoints.js')}}"></script>
  <script>
  Breakpoints();
  </script>


  <style>
  @font-face{
    font-family: Montserrat;
    src:url('{{asset("assets/fonts/montserrat/Montserrat-Light.otf")}}');
  }

  body, .site-navbar,.site-menu-item, .page, .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{
    font-family: Montserrat !important;
  }
    .site-navbar{
      background: #001555;
    }
    .site-navbar .navbar-container{
      background: #0055F1;
    }
    .site-menubar{
      background: #001555;
    }

    .navbar-default .navbar-toolbar .nav-link{
      color:#fff;
    }

    .navbar-default .navbar-toolbar .nav-link:focus, .navbar-default .navbar-toolbar .nav-link:hover{
      color:#eee;
    }

    .site-menu-item a{
      color:#fff;
    }

    .site-menu>.site-menu-item.active>a{
      color:#2285C5;
    }
    .site-menu>.site-menu-item.hover>a, .site-menu>.site-menu-item:hover>a{
      color:#2285C5;
    }

    .navbar-default .hamburger .hamburger-bar, 
    .navbar-default .hamburger:after, 
    .navbar-default .hamburger:before{
      background: #fff;
    }
    .navbar-default .navbar-toolbar>.open>.nav-link, 
    .navbar-default .navbar-toolbar>.open>.nav-link:focus, 
    .navbar-default .navbar-toolbar>.open>.nav-link:hover, 
    .navbar-default .navbar-toolbar>.show>.nav-link, 
    .navbar-default .navbar-toolbar>.show>.nav-link:focus, 
    .navbar-default .navbar-toolbar>.show>.nav-link:hover{
      color: #fff;
      background-color: rgba(0,0,0,0.05);
    }

    .site-menu>.site-menu-item.active{
      background: rgba(0,0,0,0.3);
    }

    .site-menu .site-menu-sub .site-menu-item.hover>a, 
    .site-menu .site-menu-sub .site-menu-item:hover>a{
      color:#2285C5;
    }

    .site-menubar-unfold .site-menu-category{
      color:#ddd;
    }

    .site-menu .site-menu-sub{
      padding: 10px 0;
      background: #000339;
    }
    .site-menu>.site-menu-item.open>a {
      color: #fff;
      background: #000341;
    }

    .site-menubar-unfold .site-menu-icon{
      color: #2BA5EE;
    }

    .site-menu .site-menu-sub .site-menu-item>a {
        padding: 0 30px 0 62px;
    }

    .site-menubar-fold .site-menu>.site-menu-item.hover>.site-menu-sub {
        visibility: visible;
        opacity: 1;
        background: #000339;
        border:none;
    }

    .dropdown-item:focus, 
    .dropdown-item:hover {
        text-decoration: none;
        color: #fff;
        background-color: #0055F1;
    }

    .site-menubar-unfold .site-menu>.site-menu-item>a {
      padding: 0 30px;
      line-height: 42px;
    }
    .navbar-default .navbar-toolbar .nav-link:focus, 
    .navbar-default .navbar-toolbar .nav-link:hover {
        border: none;
        background-color: rgba(0,0,0,.1);
    }

    .page-header {
        position: relative;
        padding: 15px 30px;
        margin-top: 0;
        margin-bottom: 20px;
        background: #fff;
        border-bottom: 0;
    }

    .page-title{
        font-size:20px;
        margin-bottom: 10px;
        color: #555;
    }

    .page-title>i{
      margin-right: 10px;
    }
    a:not([href]):not([tabindex]).btn{
      color:#fff;
    }
    .badge-lg{
      font-size: 14px;
    }
    label{
      color: #666;
      /*color: #0555F1;*/
      /*font-weight: 500;*/
      font-weight: 700;
      /*text-transform: uppercase;*/
    }
    .form-horizontal .form-control-label{
      text-align: left;
    }
    a:not([href]):not([tabindex]).btn.btn-default{
      color:#666;
    }
  </style>
  @yield('page-css')
</head>
<body class="animsition dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  @include('layout.navbar')

  @include('layout.sidebar')

  <div class="site-gridmenu">
    <div>
      <div>
        <ul>
          <li>
            <a href="apps/mailbox/mailbox.html">
              <i class="icon md-email"></i>
              <span>Mailbox</span>
            </a>
          </li>
          <li>
            <a href="apps/calendar/calendar.html">
              <i class="icon md-calendar"></i>
              <span>Calendar</span>
            </a>
          </li>
          <li>
            <a href="apps/contacts/contacts.html">
              <i class="icon md-account"></i>
              <span>Contacts</span>
            </a>
          </li>
          <li>
            <a href="apps/media/overview.html">
              <i class="icon md-videocam"></i>
              <span>Media</span>
            </a>
          </li>
          <li>
            <a href="apps/documents/categories.html">
              <i class="icon md-receipt"></i>
              <span>Documents</span>
            </a>
          </li>
          <li>
            <a href="apps/projects/projects.html">
              <i class="icon md-image"></i>
              <span>Project</span>
            </a>
          </li>
          <li>
            <a href="apps/forum/forum.html">
              <i class="icon md-comments"></i>
              <span>Forum</span>
            </a>
          </li>
          <li>
            <a href="index.html">
              <i class="icon md-view-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Page -->
  @yield('content')
  @yield('modal')
  
  <!-- End Page -->
  <!-- Footer -->
  <footer class="site-footer">
    <div class="site-footer-legal">© 2017 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Toba POS</a></div>
    <div class="site-footer-right">
      Crafted by <a href="idgw.net">IDGW.net</a>
    </div>
  </footer>
  <!-- Core  -->
  <script src="{{asset('assets/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
  <script src="{{asset('assets/vendor/jquery/jquery.js')}}"></script>
  <script src="{{asset('assets/vendor/tether/tether.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/bootstrap.js')}}"></script>
  <script src="{{asset('assets/vendor/animsition/animsition.js')}}"></script>
  <script src="{{asset('assets/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
  <script src="{{asset('assets/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
  <script src="{{asset('assets/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
  <script src="{{asset('assets/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
  <script src="{{asset('assets/vendor/waves/waves.js')}}"></script>
  <!-- Plugins -->
  <script src="{{asset('assets/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/intro-js/intro.js')}}"></script>
  <script src="{{asset('assets/vendor/screenfull/screenfull.js')}}"></script>
  <script src="{{asset('assets/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
  <script src="{{asset('assets/vendor/chartist/chartist.min.js')}}"></script>
  <script src="{{asset('assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js')}}"></script>
  <script src="{{asset('assets/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
  <script src="{{asset('assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
  <script src="{{asset('assets/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
  <script src="{{asset('assets/vendor/peity/jquery.peity.min.js')}}"></script>
  @yield('plugin-js')
  @yield('page-js')
  <!-- Scripts -->
  <script src="{{asset('assets/js/State.js')}}"></script>
  <script src="{{asset('assets/js/Component.js')}}"></script>
  <script src="{{asset('assets/js/Plugin.js')}}"></script>
  <script src="{{asset('assets/js/Base.js')}}"></script>
  <script src="{{asset('assets/js/Config.js')}}"></script>
  <script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
  <script src="{{asset('assets/js/Section/GridMenu.js')}}"></script>
  <script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
  <script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/menu.js')}}"></script>
  <script src="{{asset('assets/js/config/colors.js')}}"></script>
  <script src="{{asset('assets/js/config/tour.js')}}"></script>
  <!-- Page -->
  <script src="{{asset('assets/js/Site.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/asscrollable.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/slidepanel.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/switchery.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/matchheight.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/jvectormap.js')}}"></script>
  <script src="{{asset('assets/js/Plugin/peity.js')}}"></script>
  <script src="{{asset('assets/examples/js/dashboard/v1.js')}}"></script>

</body>
</html>