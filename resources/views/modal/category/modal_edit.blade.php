@extends('modal.modal_edit')

@section('edit-url')
	action="{{url('category/save')}}" 
@endsection

@section('edit-title')
	Edit Category
@endsection

@section('edit-form')
	<div class="form-group">
        <input class="form-control" type="hidden" name="id" id="editId">
        <label for="name" class="label-control text-uppercase">Category Name</label>
        <input type="text" name="name" class="form-control" placeholder="Input category here" id="editName" required>
    </div>
@endsection