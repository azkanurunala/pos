@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('category/delete')}}" 
@endsection

@section('delete-title')
	Delete Category
@endsection