@extends('modal.modal_edit')

@section('edit-url')
	action="{{url('sub-category/save')}}" 
@endsection

@section('edit-title')
	Edit Sub Category
@endsection

@section('edit-form')
	<div class="form-group">
        <input class="form-control" type="hidden" name="id" id="editId">
        <label for="name" class="label-control text-uppercase">Sub Category Name</label>
        <input type="text" name="name" class="form-control" placeholder="Input sub category here" id="editName" required>
    </div>
@endsection