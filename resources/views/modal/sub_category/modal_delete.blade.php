@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('sub-category/delete')}}" 
@endsection

@section('delete-title')
	Delete Sub Category
@endsection