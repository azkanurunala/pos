@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('product/delete')}}" 
@endsection

@section('delete-title')
	Delete Product
@endsection