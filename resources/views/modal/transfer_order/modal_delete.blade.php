@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('special-type/delete')}}" 
@endsection

@section('delete-title')
	Delete Special Type
@endsection