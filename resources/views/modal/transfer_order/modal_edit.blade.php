@extends('modal.modal_edit')

@section('edit-url')
	action="{{url('special-type/save')}}" 
@endsection

@section('edit-title')
	Edit Special Type
@endsection

@section('edit-form')
	<div class="form-group">
        <input class="form-control" type="hidden" name="id" id="editId">
        <label for="name" class="label-control text-uppercase">Special Type Name</label>
        <input type="text" name="name" class="form-control" placeholder="Input special type here" id="editName" required>
    </div>
@endsection