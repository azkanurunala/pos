@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('supplier/delete')}}" 
@endsection

@section('delete-title')
	Delete Supplier
@endsection