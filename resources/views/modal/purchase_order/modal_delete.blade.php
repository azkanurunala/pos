@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('purchase-order/delete')}}" 
@endsection

@section('delete-title')
	Delete Purchase Order
@endsection