@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('attribute/delete')}}" 
@endsection

@section('delete-title')
	Delete Attribute
@endsection