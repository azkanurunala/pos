<div class="modal modal-bordered fade" id="editModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form @yield('edit-url') method="POST">
          <div class="modal-header">
            <h5 class="modal-title" id="editTitle">@yield('edit-title')</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            @yield('edit-form')           
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="saveBtn" value="Save">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
    </div>
  </div>
</div>