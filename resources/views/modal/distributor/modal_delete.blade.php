@extends('modal.modal_delete')

@section('delete-url')
	action="{{url('distributor/delete')}}" 
@endsection

@section('delete-title')
	Delete Distributor
@endsection