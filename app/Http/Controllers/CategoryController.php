<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;

class CategoryController extends Controller
{
    public function index(Request $request){
    	return response()->view('category.index');
    }

    public function form(Request $request){
    	return response()->view('category.form');
    }

    public function detail(Request $request){
    	return response()->view('category.detail');
    }
}
