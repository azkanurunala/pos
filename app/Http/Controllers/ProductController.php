<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProductController extends Controller
{
    public function index(Request $request){
    	return response()->view('product.index');
    }

    public function form(Request $request){
    	return response()->view('product.form');
    }

    public function detail(Request $request){
    	return response()->view('product.detail');
    }
}
