<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;

class SpecialTypeController extends Controller
{
    public function index(Request $request){
    	return response()->view('special_type.index');
    }

    public function form(Request $request){
    	return response()->view('special_type.form');
    }

    public function detail(Request $request){
    	return response()->view('special_type.detail');
    }

    public function complete(Request $request){
    	return response()->view('special_type.index');
    }
}
