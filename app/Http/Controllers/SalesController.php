<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;

class SalesController extends Controller
{
    public function index(Request $request){
    	return response()->view('sales.index');
    }

    public function income(Request $request){
    	return response()->view('sales.income');
    }

    public function expense(Request $request){
    	return response()->view('sales.expense');
    }
}
