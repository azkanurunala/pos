<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/catalog', function () {
    return view('catalog.index');
});


Route::group(['prefix' => 'product', 'as'=> 'product.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'ProductController@index']);
	Route::get('form', ['as'=>'form','uses'=>'ProductController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'ProductController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'ProductController@delete']);
});

Route::group(['prefix' => 'category', 'as'=> 'category.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'CategoryController@index']);
	Route::get('form', ['as'=>'form','uses'=>'CategoryController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'CategoryController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'CategoryController@delete']);
});

Route::group(['prefix' => 'special-type', 'as'=> 'attribute.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'SpecialTypeController@index']);
	Route::get('form', ['as'=>'form','uses'=>'SpecialTypeController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'SpecialTypeController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'SpecialTypeController@delete']);
});

Route::group(['prefix' => 'attribute', 'as'=> 'special_type.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'AttributeController@index']);
	Route::get('form', ['as'=>'form','uses'=>'AttributeController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'AttributeController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'AttributeController@delete']);
});

Route::group(['prefix' => 'brand', 'as'=> 'brand.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'BrandController@index']);
	Route::get('form', ['as'=>'form','uses'=>'BrandController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'BrandController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'BrandController@delete']);
});

Route::group(['prefix' => 'discount', 'as'=> 'discount.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'DiscountController@index']);
	Route::get('form', ['as'=>'form','uses'=>'DiscountController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'DiscountController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'DiscountController@delete']);
});

Route::group(['prefix' => 'order-history', 'as'=> 'order_history.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'OrderHistoryController@index']);
	Route::get('form', ['as'=>'form','uses'=>'OrderHistoryController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'OrderHistoryController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'OrderHistoryController@delete']);
});

Route::group(['prefix' => 'purchase-order', 'as'=> 'purchase_order.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'PurchaseOrderController@index']);
	Route::get('form', ['as'=>'form','uses'=>'PurchaseOrderController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'PurchaseOrderController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'PurchaseOrderController@delete']);
});

Route::group(['prefix' => 'transfer-order', 'as'=> 'transfer_order.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'TransferOrderController@index']);
	Route::get('form', ['as'=>'form','uses'=>'TransferOrderController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'TransferOrderController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'TransferOrderController@delete']);
});

Route::group(['prefix' => 'supplier', 'as'=> 'supplier.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'SupplierController@index']);
	Route::get('form', ['as'=>'form','uses'=>'SupplierController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'SupplierController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'SupplierController@delete']);
});

Route::group(['prefix' => 'distributor', 'as'=> 'distributor.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'DistributorController@index']);
	Route::get('form', ['as'=>'form','uses'=>'DistributorController@form']);
	Route::get('detail', ['as'=>'index','uses'=>'DistributorController@detail']);
	Route::get('delete', ['as'=>'index','uses'=>'DistributorController@delete']);
});

Route::group(['prefix' => 'sales', 'as'=> 'sales.'], function(){
	Route::get('/', ['as'=>'index','uses'=>'SalesController@index']);
	Route::get('income', ['as'=>'income','uses'=>'SalesController@income']);
	Route::get('expense', ['as'=>'expense','uses'=>'SalesController@expense']);
});
// Route::get('/product', function () {
//     return view('product.index');
// });


// Route::get('/product/detail', function () {
//     return view('product.detail');
// });


// Route::get('/product/form', function () {
//     return view('product.form');
// });